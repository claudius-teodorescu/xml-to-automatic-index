use constcat::concat;
use oxrdf::NamedNodeRef;
use oxttl::TurtleParser;

pub const ONTOLOGY_BASE_IRI: &str = "https://kuberam.ro/ontologies/text-index#";
const HEADWORDS_INDEX_CLASS: &str = concat!("<", ONTOLOGY_BASE_IRI, "HeadwordsIndex", ">");
const WORDS_INDEX_CLASS: &str = concat!("<", ONTOLOGY_BASE_IRI, "WordsIndex", ">");
const DATES_INDEX_CLASS: &str = concat!("<", ONTOLOGY_BASE_IRI, "DatesIndex", ">");
const KEYWORDS_INDEX_CLASS: &str = concat!("<", ONTOLOGY_BASE_IRI, "KeywordsIndex", ">");
const SELECTOR_PROPERTY: &str = "selector";
const TYPE_PROPERTY: &str = "type";

#[derive(Debug, Clone)]
pub struct Metadata {
    pub selector: String,
    pub predicate: String,
}

impl Metadata {
    pub fn new(input_metadata: &str) -> Metadata {
        let indexing_selector =
            NamedNodeRef::new(concat!(ONTOLOGY_BASE_IRI, SELECTOR_PROPERTY)).unwrap();
        let indexing_type = NamedNodeRef::new(concat!(ONTOLOGY_BASE_IRI, TYPE_PROPERTY)).unwrap();

        let mut selector = "".to_string();
        let mut r#type = "".to_string();

        for triple in TurtleParser::new().for_reader(input_metadata.as_bytes()) {
            let triple = triple.unwrap();
            if triple.predicate == indexing_selector {
                selector = triple.object.to_string().replace("\"", "")
            }

            if triple.predicate == indexing_type {
                r#type = triple.object.to_string();
            }
        }

        let predicate: String = match r#type.as_str() {
            HEADWORDS_INDEX_CLASS => "i:headword".to_string(),
            WORDS_INDEX_CLASS => "i:word".to_string(),
            KEYWORDS_INDEX_CLASS => "i:keyword".to_string(),
            DATES_INDEX_CLASS => "i:date".to_string(),
            _ => "".to_string(),
        };

        Metadata {
            selector,
            predicate,
        }
    }
}

#[test]
pub fn test_1() {
    let input_metadata = r#"
    @prefix i: <https://kuberam.ro/ontologies/text-index#> .

    <https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/metadata.ttl> i:metadataFor <https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/index.ttl> ;
        i:type i:HeadwordsIndex ;
        i:title "Index de cuvinte-titlu" ;
        i:textBaseURL <https://solirom-citada.gitlab.io/data/text/> ;
        i:selector "/*/*[local-name() = 'orth']" .
    "#;

    let metadata = Metadata::new(input_metadata);

    println!("{:?}", metadata.selector);
}
// Metadata { selector: "/*/*[local-name() = 'orth']", predicate: "" }
// Metadata2 { selector: "\"/*/*[local-name() = 'orth']\"", predicate: "i:headword" }
