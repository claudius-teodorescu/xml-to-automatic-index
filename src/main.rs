mod config;

use clap::{Arg, Command};
use rio_api::formatter::TriplesFormatter;
use rio_api::model::{Literal, NamedNode, Triple};
use rio_turtle::TurtleFormatter;
use std::path::{Path, PathBuf};
use std::{env, fs};
use sxd_document::parser;
use sxd_xpath::{Context, Factory, Value};
use url::Url;

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("config_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(
            Arg::new("documents_base_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(2)
                .required(true),
        )
        .arg(Arg::new("documents_glob").index(3).required(true))
        .arg(
            Arg::new("indexes_dir_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(4)
                .required(true),
        )
        .arg(Arg::new("index_abbreviation").index(5).required(true))
        .arg(Arg::new("documents_base_url").index(6))
        .get_matches();

    let config_file_path: &PathBuf = cli_matches
        .get_one("config_file_path")
        .expect("`config_file_path`is required");
    let documents_base_path: &PathBuf = cli_matches
        .get_one("documents_base_path")
        .expect("`documents_base_path`is required");
    let documents_glob_arg: &String = cli_matches
        .get_one("documents_glob")
        .expect("`documents_glob`is required");
    let documents_glob: glob::Pattern =
        glob::Pattern::new(documents_glob_arg.as_str()).expect("invalid `documents_glob`");
    let indexes_dir_path: &PathBuf = cli_matches
        .get_one("indexes_dir_path")
        .expect("`indexes_dir_path`is required");
    let index_abbreviation: &String = cli_matches
        .get_one("index_abbreviation")
        .expect("`index_abbreviation`is required");
    let documents_base_url: Option<&String> = cli_matches.get_one("documents_base_url");

    let input_metadata_file_contents =
        fs::read_to_string(config_file_path).expect("Failed to read config file");
    let input_metadata: crate::config::Metadata =
        crate::config::Metadata::new(&input_metadata_file_contents);

    process_documents(
        documents_base_path,
        documents_glob,
        indexes_dir_path,
        input_metadata,
        index_abbreviation,
        documents_base_url,
    );
}

fn process_documents(
    documents_base_path: &Path,
    documents_glob: glob::Pattern,
    indexes_dir_path: &Path,
    input_metadata: crate::config::Metadata,
    index_abbreviation: &str,
    documents_base_url: Option<&String>,
) {
    let documents_path = documents_base_path.join(documents_glob.as_str());

    // initialisations
    let mut subjects_objects: Vec<(String, String)> = Vec::new();

    for document_path in glob::glob(documents_path.to_str().unwrap())
        .expect("Failed to read glob pattern")
        .filter_map(Result::ok)
    {
        process_document(
            document_path,
            documents_base_path,
            documents_base_url,
            input_metadata.clone(),
            &mut subjects_objects,
        );
    }

    // save the indexes
    // prolog for index file
    let mut prolog = format!("@prefix i: <{}> .\n", crate::config::ONTOLOGY_BASE_IRI)
        .as_bytes()
        .to_vec();

    // generate the triples
    let mut turtle_formatter = TurtleFormatter::new(Vec::default());
    for (subject, object) in subjects_objects.into_iter() {
        turtle_formatter
            .format(&Triple {
                subject: NamedNode {
                    iri: subject.as_str(),
                }
                .into(),
                predicate: NamedNode {
                    iri: input_metadata.predicate.clone().as_str(),
                }
                .into(),
                object: Literal::Simple {
                    value: object.as_str(),
                }
                .into(),
            })
            .expect("error formatting triple");
    }

    // write the subjects file
    let mut subjects_document = turtle_formatter
        .finish()
        .expect("cannot finish terms generation");
    prolog.append(&mut subjects_document);

    let triples_file_dir_path = indexes_dir_path.join(index_abbreviation);
    fs::create_dir_all(&triples_file_dir_path).unwrap();
    fs::write(triples_file_dir_path.join("index.ttl"), &prolog).expect("Write file.");
}

fn process_document(
    document_path: PathBuf,
    documents_base_path: &Path,
    documents_base_url: Option<&String>,
    input_metadata: crate::config::Metadata,
    subjects_objects: &mut Vec<(String, String)>,
) {
    // get the file contents
    let file_contents: String = fs::read_to_string(&document_path)
        .expect("cannot read file")
        .parse()
        .expect("cannot parse file");

    // get the relative document path
    let document_relative_path = get_document_relative_path(documents_base_path, &document_path);
    let document_url = if let Some(documents_base_url) = documents_base_url {
        let document_url = format!("{}{}", documents_base_url, document_relative_path);
        let parsed_document_url = Url::parse(document_url.as_str()).unwrap();

        parsed_document_url.as_str().to_string()
    } else {
        document_relative_path
    };

    // parse the document contents
    let package = parser::parse(file_contents.as_str()).expect("failed to parse XML");
    let document = package.as_document();

    let mut context = Context::new();
    context.set_namespace("xml", "http://www.w3.org/XML/1998/namespace");

    // get the subject
    let selector = Factory::new()
        .build(input_metadata.selector.as_str())
        .expect("Invalid XPath")
        .expect("No XPath");
    let subject_value = selector
        .evaluate(&context, document.root())
        .expect("Cannot evaluate XPath");

    if let Value::Nodeset(ref nodeset) = subject_value {
        nodeset.iter().for_each(|node| {
            let subject = node.string_value();

            if subject.len() > 0 {
                subjects_objects.push((document_url.to_owned(), subject.to_owned()));
            }
        });
    }
}

fn get_document_relative_path(documents_base_path: &Path, document_path: &PathBuf) -> String {
    let mut text_file_relative_path_buffer = PathBuf::new();
    document_path
        .components()
        .skip(documents_base_path.components().collect::<Vec<_>>().len())
        .for_each(|component| text_file_relative_path_buffer.push(component));
    let text_file_relative_path = text_file_relative_path_buffer.to_str().unwrap();

    text_file_relative_path.to_owned()
}

#[test]
pub fn citada_1() {
    let index_abbreviation = "bibl-dates";

    let repository_base_path =
        Path::new("/home/claudius/workspace/repositories/git/gitlab.com/citada/data/");

    let documents_base_path = repository_base_path.join("contents/text/");
    let documents_glob = glob::Pattern::new("**/*.xml").expect("wrong glob pattern");
    let indexes_dir_path = repository_base_path.join("public/peritext/indexes/");

    let input_metadata_file_contents = fs::read_to_string(repository_base_path.join(format!(
        "contents/peritext/indexes/{}/metadata.ttl",
        index_abbreviation
    )))
    .expect("Failed to read file");
    let input_metadata: crate::config::Metadata =
        crate::config::Metadata::new(input_metadata_file_contents.as_str());
    let documents_base_url =
        &"https://citada.solirom.ro/text/".to_string();

    process_documents(
        documents_base_path.as_path(),
        documents_glob,
        indexes_dir_path.as_path(),
        input_metadata,
        index_abbreviation,
        Some(documents_base_url),
    );
}
